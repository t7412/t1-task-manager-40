package ru.t1.chubarov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.chubarov.tm.api.service.ICommandService;
import ru.t1.chubarov.tm.command.AbstractCommand;
import ru.t1.chubarov.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    public ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    public ISystemEndpoint getSystemEndpoint() {
        return getServiceLocator().getSystemEndpoint();
    }

    @Nullable
    public String getArgument() {
        return null;
    }

    @Nullable
    public Role[] getRoles() {
        return null;
    }

}
