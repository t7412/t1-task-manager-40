package ru.t1.chubarov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.chubarov.tm.command.AbstractCommand;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

    @NotNull
    public static final String FILE_BACKUP = "./data_backup.base64";

}
