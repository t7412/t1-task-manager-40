package ru.t1.chubarov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chubarov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.chubarov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.chubarov.tm.api.service.ITokenService;
import ru.t1.chubarov.tm.dto.request.*;
import ru.t1.chubarov.tm.enumerated.ProjectSort;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.marker.SoapCategory;
import ru.t1.chubarov.tm.model.Project;
import ru.t1.chubarov.tm.model.Task;
import ru.t1.chubarov.tm.service.TokenService;

import java.util.List;

@Category(SoapCategory.class)
public class ProjectEndpointTest {

    private static int NUMBER_OF_ENTRIES = 3;

    @NotNull
    private final ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance();

    @Nullable
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Nullable
    private final IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance();

    @NotNull
    private final ITokenService tokenService = new TokenService();

    @Nullable
    private String token;

    @Nullable
    private String projectId;

    @Before
    public void initTest() {
        @NotNull final String login = "user";
        @NotNull final String password = "user";
        @NotNull final UserLoginRequest request = new UserLoginRequest(login, password);
        request.setLogin(login);
        request.setPassword(password);
        token = authEndpoint.login(request).getToken();
        tokenService.setToken(token);

        @NotNull final ProjectClearRequest requestClear = new ProjectClearRequest(token);
        projectEndpoint.clearProject(requestClear);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final String name = "Project Name " + i;
            @NotNull final String description = "Description project test " + i;
            @NotNull final ProjectCreateRequest projectrequest = new ProjectCreateRequest(name, description, token);
            if (i == 1) projectId = projectEndpoint.createProject(projectrequest).getProject().getId();
            else projectEndpoint.createProject(projectrequest);
        }
    }

    @Test
    @Category(SoapCategory.class)
    public void testSize() {
        @NotNull final ProjectSort sort = ProjectSort.toSort("BY_NAME");
        @NotNull final ProjectShowRequest request = new ProjectShowRequest(sort, token);
        @Nullable final List<Project> projects = projectEndpoint.listProject(request).getProjects();
        Assert.assertEquals(NUMBER_OF_ENTRIES, projects.size());
    }


    @Test
    public void testCreateProject() {
        @NotNull final String name = "New Project Name";
        @NotNull final String description = "New description test";
        projectEndpoint.createProject(new ProjectCreateRequest(name, description, token));

        @Nullable final List<Project> projects = projectEndpoint.listProject(new ProjectShowRequest(ProjectSort.toSort("BY_NAME"), token)).getProjects();
        Assert.assertEquals("Project Name 1", projects.get(0).getName());
        Assert.assertEquals(name, projects.get(3).getName());
        Assert.assertEquals(description, projects.get(3).getDescription());
        Assert.assertEquals(Status.NOT_STARTED, projects.get(3).getStatus());
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, projects.size());
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.createProject(new ProjectCreateRequest("", description, token)));
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.createProject(new ProjectCreateRequest(null, description, token)));
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.createProject(new ProjectCreateRequest(name, null, token)));
        Assert.assertThrows(
                Exception.class, () -> projectEndpoint.createProject(new ProjectCreateRequest(name, description, null)));
    }

    @Test
    public void testListProject() {
        @Nullable final List<Project> projects = projectEndpoint.listProject(new ProjectShowRequest(ProjectSort.toSort("BY_NAME"), token)).getProjects();
        Assert.assertEquals("Project Name 1", projects.get(0).getName());
        Assert.assertEquals("Project Name 2", projects.get(1).getName());
        Assert.assertEquals("Project Name 3", projects.get(2).getName());
        Assert.assertEquals(NUMBER_OF_ENTRIES, projects.size());
        @Nullable final List<Project> projects2 = projectEndpoint.listProject(new ProjectShowRequest(null, token)).getProjects();
        Assert.assertEquals("Project Name 1", projects2.get(0).getName());
        Assert.assertEquals("Project Name 2", projects2.get(1).getName());
        Assert.assertEquals("Project Name 3", projects2.get(2).getName());
    }

    @Test
    public void testSClear() {
        projectEndpoint.clearProject(new ProjectClearRequest(token));
        @NotNull final ProjectShowRequest request = new ProjectShowRequest(ProjectSort.toSort("BY_NAME"), token);
        @Nullable final List<Project> projects = projectEndpoint.listProject(request).getProjects();
        Assert.assertNull(projects);
    }

    @Test
    public void testChangeStatusById() {
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(token);
        request.setId(projectId);
        request.setStatus(Status.IN_PROGRESS);
        @Nullable final Project project = projectEndpoint.changeProjectStatusById(request).getProject();
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
        request.setId(projectId);
        request.setStatus(Status.COMPLETED);
        @Nullable final Project project_complete = projectEndpoint.changeProjectStatusById(request).getProject();
        Assert.assertEquals(Status.COMPLETED, project_complete.getStatus());
        request.setStatus(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.changeProjectStatusById(request));
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final List<Project> projects0 = projectEndpoint.listProject(new ProjectShowRequest(ProjectSort.toSort("BY_NAME"), token)).getProjects();
        Assert.assertEquals(NUMBER_OF_ENTRIES, projects0.size());
        @NotNull final ProjectRemoveByIdRequest request = new ProjectRemoveByIdRequest(token);
        request.setId(projectId);
        projectEndpoint.removeProjectById(request);
        @Nullable final List<Project> projects = projectEndpoint.listProject(new ProjectShowRequest(ProjectSort.toSort("BY_NAME"), token)).getProjects();
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, projects.size());
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(null));
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.removeProjectById(request));
    }

    @Test
    public void testGetById() {
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(token);
        request.setId(projectId);
        @NotNull final Project project = projectEndpoint.getProjectById(request).getProject();
        Assert.assertEquals("Project Name 1", project.getName());
        Assert.assertEquals("Description project test 1", project.getDescription());
        Assert.assertEquals(projectId, project.getId());
        request.setId(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(request));
        request.setId("failId");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.getProjectById(request));
    }

    @Test
    public void testUpdateById() {
        @NotNull final ProjectUpdateByIdRequest request = new ProjectUpdateByIdRequest(token);
        request.setId(projectId);
        request.setName("New Project Name");
        request.setDescription("New Project Description");
        projectEndpoint.updateProjectById(request);
        @Nullable final List<Project> projects = projectEndpoint.listProject(new ProjectShowRequest(ProjectSort.toSort("BY_NAME"), token)).getProjects();
        Assert.assertEquals("New Project Name", projects.get(0).getName());
        Assert.assertEquals("New Project Description", projects.get(0).getDescription());
        Assert.assertEquals(NUMBER_OF_ENTRIES, projects.size());

        request.setId(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(request));
        request.setId("fail_Id");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(request));
        request.setId(projectId);
        request.setName("");
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(request));
        request.setName(null);
        Assert.assertThrows(Exception.class, () -> projectEndpoint.updateProjectById(request));
        request.setName("New Project Name");
        request.setDescription("");
        projectEndpoint.updateProjectById(request);
        @Nullable final List<Project> projects_desc = projectEndpoint.listProject(new ProjectShowRequest(ProjectSort.toSort("BY_NAME"), token)).getProjects();
        Assert.assertEquals("New Project Name", projects_desc.get(0).getName());
        Assert.assertEquals("", projects_desc.get(0).getDescription());
    }

    @Test
    public void testTaskBindToProject() {
        @NotNull final TaskClearRequest requestClear = new TaskClearRequest(token);
        taskEndpoint.clearTask(requestClear);
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest("TaskNew", "test for Task bind by project", token);
        @Nullable final Task task = taskEndpoint.createTask(requestCreate).getTask();

        @NotNull final ProjectTaskBindToProjectRequest requestBind = new ProjectTaskBindToProjectRequest(token);
        requestBind.setTaskId(task.getId());
        requestBind.setProjectId(projectId);
        projectEndpoint.bindTaskToProject(requestBind);

        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(token);
        request.setProjectId(projectId);
        @NotNull final List<Task> tasks = taskEndpoint.listTaskByProjectId(request).getTasks();
        Assert.assertEquals("TaskNew", tasks.get(0).getName());
        Assert.assertEquals("test for Task bind by project", tasks.get(0).getDescription());
        Assert.assertEquals(task.getId(), tasks.get(0).getId());
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void testTaskUnbindToProject() {
        @NotNull final TaskClearRequest requestClear = new TaskClearRequest(token);
        taskEndpoint.clearTask(requestClear);
        @NotNull final TaskCreateRequest requestCreate = new TaskCreateRequest("TaskNew", "test for Task bind by project", token);
        @Nullable final Task task = taskEndpoint.createTask(requestCreate).getTask();

        @NotNull final ProjectTaskBindToProjectRequest requestBind = new ProjectTaskBindToProjectRequest(token);
        requestBind.setTaskId(task.getId());
        requestBind.setProjectId(projectId);
        projectEndpoint.bindTaskToProject(requestBind);

        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(token);
        request.setProjectId(projectId);
        @NotNull List<Task> tasks = taskEndpoint.listTaskByProjectId(request).getTasks();
        Assert.assertEquals("TaskNew", tasks.get(0).getName());
        Assert.assertEquals("test for Task bind by project", tasks.get(0).getDescription());
        Assert.assertEquals(task.getId(), tasks.get(0).getId());
        Assert.assertEquals(1, tasks.size());

        @NotNull final ProjectTaskUnbindToProjectRequest requestUnbind = new ProjectTaskUnbindToProjectRequest(token);
        requestUnbind.setTaskId(task.getId());
        requestUnbind.setProjectId(projectId);
        projectEndpoint.unbindTaskToProject(requestUnbind);

        tasks = taskEndpoint.listTaskByProjectId(request).getTasks();
        Assert.assertNull(tasks);
    }

}
