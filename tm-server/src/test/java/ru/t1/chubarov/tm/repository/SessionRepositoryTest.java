package ru.t1.chubarov.tm.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chubarov.tm.api.property.IDatabaseProperty;
import ru.t1.chubarov.tm.api.repository.ISessionRepository;
import ru.t1.chubarov.tm.api.service.IConnectionService;
import ru.t1.chubarov.tm.marker.UnitCategory;
import ru.t1.chubarov.tm.model.Session;
import ru.t1.chubarov.tm.service.ConnectionService;
import ru.t1.chubarov.tm.service.PropertyService;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Category(UnitCategory.class)
public class SessionRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 2;

    @NotNull
    private final String userIdFirstSession = UUID.randomUUID().toString();

    @NotNull
    private final String userIdSecondSession = UUID.randomUUID().toString();

    @NotNull
    private final String firstSessionId = UUID.randomUUID().toString();

    @NotNull
    private List<Session> sessionList;

    @NotNull
    final IDatabaseProperty databaseProperty = new PropertyService();

    @NotNull
    final IConnectionService connectionService = new ConnectionService(databaseProperty);

    @NotNull
    final SqlSession sqlSession = connectionService.getSqlSession();
    @NotNull
    final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);


    @SneakyThrows
    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        @NotNull final Session session1 = new Session();
        session1.setUserId(userIdFirstSession);
        session1.setDate(new Date());
        session1.setId(firstSessionId);
        sessionRepository.add(session1);
        sessionList.add(session1);
        @NotNull final Session session2 = new Session();
        session2.setUserId(userIdSecondSession);
        session2.setDate(new Date());
        sessionRepository.add(session2);
        sessionList.add(session2);
        sqlSession.commit();
    }

    @After
    public void finish() throws Exception {
        System.out.println("Session End After.");
        sessionList.clear();
        sessionRepository.clear();
        sqlSession.commit();
        sqlSession.close();
    }

    @SneakyThrows
    @Test
    public void testAdd() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
        @NotNull final Session session1 = new Session();
        session1.setUserId(userIdFirstSession);
        session1.setDate(new Date());
        sessionRepository.add(session1);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, sessionRepository.getSize());
        @NotNull final Session session2 = new Session();
        session2.setUserId(userIdFirstSession);
        session2.setDate(new Date());
        sessionRepository.add(session2);
        Assert.assertEquals(NUMBER_OF_ENTRIES + 2, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemove() {
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
        @NotNull final Session session =sessionRepository.findOneById(userIdFirstSession, firstSessionId);
        sessionRepository.remove(session);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testRemoveOneById() {
        sessionRepository.removeOneById(userIdFirstSession, "empty_sessionId");
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
        sessionRepository.removeOneById(userIdFirstSession, firstSessionId);
        Assert.assertEquals(NUMBER_OF_ENTRIES - 1, sessionRepository.getSize());
    }

    @SneakyThrows
    @Test
    public void testExistsById() {
        Assert.assertTrue(sessionRepository.existsById(userIdFirstSession, firstSessionId)>0);
        Assert.assertTrue(sessionRepository.existsById(userIdSecondSession, sessionList.get(1).getId())>0);
        Assert.assertFalse(sessionRepository.existsById(userIdFirstSession,"empty_id")>0);
    }

    @SneakyThrows
    @Test
    public void testFindOneById() {
        Assert.assertNotNull(sessionRepository.findOneById(userIdFirstSession, firstSessionId));
    }

    @SneakyThrows
    @Test
    public void testFindOneByIdNegative() {
        Assert.assertNull(sessionRepository.findOneById(userIdFirstSession, "empty_id"));
    }

    @Test
    public void testFindOAll() throws Exception {
        @NotNull final List<Session> SessionList = sessionRepository.findAll();
        Assert.assertEquals(2, SessionList.size());
        @NotNull final List<Session> actualSessionList = sessionRepository.findAllByUser(userIdFirstSession);
        Assert.assertEquals(1, actualSessionList.size());
    }

    @SneakyThrows
    @Test
    public void testGetSizeForUser() {
        Assert.assertEquals(1, sessionRepository.getSizeByUser(userIdFirstSession));
    }

}
