package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.exception.AbstractException;
import ru.t1.chubarov.tm.model.Session;
import ru.t1.chubarov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@NotNull String login, @NotNull String password, @NotNull String email) throws AbstractException, Exception;

    String login(@Nullable String login, @Nullable String password) throws Exception;

    void logout(@Nullable Session session) throws Exception;

    @NotNull
    Session validateToken(@Nullable String token);

    void invalidate(@NotNull Session session);

}
