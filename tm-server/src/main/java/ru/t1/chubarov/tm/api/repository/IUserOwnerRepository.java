package ru.t1.chubarov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.model.AbstractUserOwnerModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnerRepository<M extends AbstractUserOwnerModel> extends IRepository<M> {

    @Nullable
    List<M> findAll(@NotNull String userId) throws Exception;

    @Nullable
    List<M> findAll(@NotNull String userId, @NotNull Comparator<M> comparator) throws Exception;

    @Nullable
    M findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    @Nullable
    M findOneByIndex(@NotNull String userId, @Nullable Integer index) throws Exception;

    @Nullable
    M remove(@NotNull String userId, @NotNull  M model) throws Exception;

    @Nullable
    M removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    @Nullable
    M removeOneByIndex(@Nullable String userId, @Nullable Integer index) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @Nullable
    M add(@Nullable String userId, @Nullable M model) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;



}
