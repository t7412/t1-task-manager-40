package ru.t1.chubarov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Status;
import ru.t1.chubarov.tm.model.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;


public interface IProjectService {

    @NotNull
    Project create(@NotNull String userId, @NotNull String name) throws Exception;

    @NotNull
    Project create(@NotNull String userId, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    Project updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws Exception;

    @NotNull
    Project changeProjectStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws Exception;

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

    @NotNull
    Project add(@Nullable String userId, @Nullable Project model) throws Exception;

    @Nullable
    List<Project> findAll(@Nullable String userId) throws Exception;

    @NotNull
    List<Project> findAll() throws Exception;

    @Nullable
    List<Project> findAll(@Nullable String userId, @Nullable Comparator<Project> comparator) throws Exception;

    @NotNull
    Project findOneById(@NotNull String userId, @Nullable String id) throws Exception;

    @NotNull
    Project remove(@Nullable String userId, @Nullable Project model) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    Project removeOneById(@Nullable String userId, @Nullable String id) throws Exception;

    int getSize() throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    @NotNull
    Collection<Project> add(@NotNull Collection<Project> models) throws Exception;

    @NotNull
    Collection<Project> set(@NotNull Collection<Project> models) throws Exception;

    void clear();
}
