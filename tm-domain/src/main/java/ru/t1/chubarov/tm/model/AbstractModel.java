package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;
@Getter
@Setter
public abstract class AbstractModel implements Serializable {

    @NotNull
    private String id = UUID.randomUUID().toString();

//    @NotNull
//    public String getId() {
//        return id;
//    }
//
//    public void setId(@NotNull String id) {
//        this.id = id;
//    }

}
