package ru.t1.chubarov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.chubarov.tm.api.model.IWBS;
import ru.t1.chubarov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Project extends AbstractUserOwnerModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Project(@NotNull String name, @NotNull Status status) {
        this.name = name;
        this.status = status;
    }
//
//    @NotNull
//    public String getName() {
//        return name;
//    }
//
//    public void setName(@NotNull String name) {
//        this.name = name;
//    }
//
//    @NotNull
//    public String getDescription() {
//        return description;
//    }
//
//    public void setDescription(@NotNull String description) {
//        this.description = description;
//    }
//
//    @NotNull
//    public Status getStatus() {
//        return status;
//    }
//
//    public void setStatus(@NotNull Status status) {
//        this.status = status;
//    }
//
//    @NotNull
//    @Override
//    public Date getCreated() {
//        return created;
//    }
//
//    @Override
//    public void setCreated(@NotNull Date created) {
//        this.created = created;
//    }

}
